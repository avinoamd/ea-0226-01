/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include <stdio.h>
#include "BarCodeSerialComm.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
modbusHandler_t ModbusH;
uint16_t ModbusDATA[41];
IOHandler_t  IOHandle;
BarCodeHandler_t BarCodeSerial1;
BarCodeHandler_t BarCodeSerial2;
BarCodeHandler_t BarCodeSerial3;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_UART4_Init();
  MX_UART5_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_TIM1_Init();
  /* USER CODE BEGIN 2 */

  HAL_GPIO_WritePin( MCU_OUT_1_GPIO_Port, MCU_OUT_1_Pin, 1);
  HAL_GPIO_WritePin( MCU_OUT_2_GPIO_Port, MCU_OUT_2_Pin, 1);
  HAL_GPIO_WritePin( MCU_OUT_3_GPIO_Port, MCU_OUT_3_Pin, 1);
  HAL_GPIO_WritePin( MCU_OUT_4_GPIO_Port, MCU_OUT_4_Pin, 1);
  HAL_GPIO_WritePin( MCU_OUT_5_GPIO_Port, MCU_OUT_5_Pin, 1);
  HAL_GPIO_WritePin( MCU_OUT_6_GPIO_Port, MCU_OUT_6_Pin, 1);

  /* Modbus Slave initialization over RS-485*/
	ModbusH.uModbusType = MB_SLAVE;
	ModbusH.port =  &huart1; // This is the UART port connected to STLINK
	ModbusH.u8id = 5; //slave ID
	ModbusH.u16timeOut = 1000;
	ModbusH.EN_Port = NULL;
	ModbusH.EN_Pin = (uint32_t)NULL; // RS485 Enable
	ModbusH.u32time = 0;
	ModbusH.u16regs = ModbusDATA;
	ModbusH.u16regsize= sizeof(ModbusDATA)/sizeof(ModbusDATA[0]);
	ModbusH.xTypeHW	=	USART_HW;

//	osKernelInitialize();  /* Call init function for freertos objects (in freertos.c) */
//	MX_FREERTOS_Init();

	//Initialize Modbus library
	ModbusInit(&ModbusH);
	//Start capturing traffic on serial Port
	ModbusStart(&ModbusH);

	// HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	// HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);

   /* IO Task initialization */
   IOHandle.Data	=	&(ModbusDATA[0]);
   //IOHandle.myTaskInputOutput	= &(ModbusH.myTaskModbusAHandle);
   IOHandle.IOSemap_Handle		= &(ModbusH.ModBusSphrHandle);
   InputOutputInit(&IOHandle);

   /* BarCode 1 Serial communication initialization over RS-232*/
   BarCodeSerial1.Data = ModbusDATA;
   BarCodeSerial1.BarCodeSemap_HandleRx = &(ModbusH.ModBusSphrHandle);
   BarCodeSerial1.BarCodeSemap_HandleTx = &(ModbusH.ModBusSphrHandle);
   BarCodeSerial1.port = &huart3;
   BarCodeSerial1.ReadRequestMap_ReadReq = eReadRequestMap_Barcode_1_ReadReq;
   BarCodeSerial1.ReadRequestMap_ReadDisableReq = eReadRequestMap_Barcode_1_AllowReadDisableReq;
   BarCodeSerial1.ReadRequestMap_ResetReq = eReadRequestMap_Barcode_1_ResetReq;
   BarCodeSerial1.ModbusMap_ReadingMSB = eModbusMap_BarCode_1_ReadingMSB;
   BarCodeSerial1.ModbusMap_ReadingLSB = eModbusMap_BarCode_1_ReadingLSB;
   BarCodeSerial1.ModbusMap_ToteOrientaion = eModbusMap_BarCode_1_ToteOrientaion;
   BarCodeSerial1.ReadRequestMap_DisableGreenLed = eReadRequestMap_Barcode_1_BarCodeDisableGreenLed;
   //Initialize BarCode serial communication library
   BarCodeSerialCommInit(&BarCodeSerial1);
   //Start capturing traffic on serial Port
   BarCodeSerialCommStart(&BarCodeSerial1);

   /* BarCode 2 Serial communication initialization over RS-232*/
   BarCodeSerial2.Data = ModbusDATA;
   BarCodeSerial2.BarCodeSemap_HandleRx = &(ModbusH.ModBusSphrHandle);
   BarCodeSerial2.BarCodeSemap_HandleTx = &(ModbusH.ModBusSphrHandle);
   BarCodeSerial2.port = &huart4;
   BarCodeSerial2.ReadRequestMap_ReadReq = eReadRequestMap_Barcode_2_ReadReq;
   BarCodeSerial2.ReadRequestMap_ReadDisableReq = eReadRequestMap_Barcode_2_AllowReadDisableReq;
   BarCodeSerial2.ReadRequestMap_ResetReq = eReadRequestMap_Barcode_2_ResetReq;
   BarCodeSerial2.ModbusMap_ReadingMSB = eModbusMap_BarCode_2_ReadingMSB;
   BarCodeSerial2.ModbusMap_ReadingLSB = eModbusMap_BarCode_2_ReadingLSB;
   BarCodeSerial2.ModbusMap_ToteOrientaion = eModbusMap_BarCode_2_ToteOrientaion;
   BarCodeSerial2.ReadRequestMap_DisableGreenLed = eReadRequestMap_Barcode_2_BarCodeDisableGreenLed;
   //Initialize BarCode serial communication library
   BarCodeSerialCommInit(&BarCodeSerial2);
   //Start capturing traffic on serial Port
   BarCodeSerialCommStart(&BarCodeSerial2);

   /* BarCode 2 Serial communication initialization over RS-232*/
   BarCodeSerial3.Data = ModbusDATA;
   BarCodeSerial3.BarCodeSemap_HandleRx = &(ModbusH.ModBusSphrHandle);
   BarCodeSerial3.BarCodeSemap_HandleTx = &(ModbusH.ModBusSphrHandle);
   BarCodeSerial3.port = &huart5;
   BarCodeSerial3.ReadRequestMap_ReadReq = eReadRequestMap_Barcode_3_ReadReq;
   BarCodeSerial3.ReadRequestMap_ReadDisableReq = eReadRequestMap_Barcode_3_AllowReadDisableReq;
   BarCodeSerial3.ReadRequestMap_ResetReq = eReadRequestMap_Barcode_3_ResetReq;
   BarCodeSerial3.ModbusMap_ReadingMSB = eModbusMap_BarCode_3_ReadingMSB;
   BarCodeSerial3.ModbusMap_ReadingLSB = eModbusMap_BarCode_3_ReadingLSB;
   BarCodeSerial3.ModbusMap_ToteOrientaion = eModbusMap_BarCode_3_ToteOrientaion;
   BarCodeSerial3.ReadRequestMap_DisableGreenLed = eReadRequestMap_Barcode_3_BarCodeDisableGreenLed;
   //Initialize BarCode serial communication library
   BarCodeSerialCommInit(&BarCodeSerial3);
   //Start capturing traffic on serial Port
   BarCodeSerialCommStart(&BarCodeSerial3);

  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();
  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 12;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM2 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM2) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

