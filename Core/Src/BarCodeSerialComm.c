/*
 * ScaleSerialComm.c
 *
 *  Created on: Sep 14, 2020
 *      Author: segev.gofin
 */
#include "FreeRTOS.h"
#include "cmsis_os2.h"
#include "task.h"
#include "queue.h"
#include "usart.h"
#include "BarCodeSerialComm.h"
#include "timers.h"
#include "semphr.h"
#include "main.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// Bit macro
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define bitSet(value, bit) ((value) |= (1UL << (bit)))
#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitvalue) ((bitvalue) ? bitSet(value, bit) : bitClear(value, bit))

enum Orientation {
	eOrientation_LA=0,
	eOrientation_RA
};

BarCodeHandler_t *tHandlers[MAX_M_HANDLERS];

//Task ScaleSerialComm
uint8_t numberOFHandlers = 0;

///Queue Serial communication RX
//osMessageQueueId_t QueueModbusHandle;
const osMessageQueueAttr_t QueueBarCode_attributes = {
       .name = "QueueScale"
};

//Sempahore to access the modebus Data
const osSemaphoreAttr_t BarCodeSemap_attributes = {
    .name = "BarCodeSerialCommSemap"
};

/* Definitions for IO_task */
//osThreadId_t Scale_taskHandle;
const osThreadAttr_t BarCodeRx_task_attributes = {
  .name = "BarCodeRxSerialComm_task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
const osThreadAttr_t BarCodeTx_task_attributes = {
  .name = "BarCodeTxSerialComm_task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/**
 * Initialization for Serial Comm task
 * */
void BarCodeSerialCommInit(BarCodeHandler_t * BarCodeTaskHandler)
{
	//Create Queue and Thread Rx
	BarCodeTaskHandler->QueueSerialHandle = osMessageQueueNew (MAX_BUFFER, sizeof(uint8_t), &QueueBarCode_attributes);
	BarCodeTaskHandler->BarCodeCommTaskRx = osThreadNew(StartBarCodeRx_task, BarCodeTaskHandler, &BarCodeRx_task_attributes);
	/* Create Queue and Thread Tx */
	BarCodeTaskHandler->BarCodeCommTaskTx = osThreadNew(StartBarCodeTx_task,BarCodeTaskHandler,&BarCodeTx_task_attributes);
	if (BarCodeTaskHandler->BarCodeCommTaskTx == NULL)
		BarCodeTaskHandler->BarCodeCommTaskTx = osThreadNew(StartBarCodeTx_task,BarCodeTaskHandler,&BarCodeTx_task_attributes);
	//Create Semaphore DataRX
	 //vSemaphoreCreateBinary(SemaphoreDataRX);
	 //Create timer T35

	BarCodeTaskHandler->xTimerT35 = xTimerCreate("TimerT35",         // Just a text name, not used by the kernel.
			  	  	  	  	  	  	  	5 ,     // The timer period in ticks.
	                                    pdFALSE,         // The timers will auto-reload themselves when they expire.
										( void * )BarCodeTaskHandler->xTimerT35,     // Assign each timer a unique id equal to its array index.
	                                    (TimerCallbackFunction_t) vBarCodeTimerCallbackT35     // Each timer calls the same callback when it expires.
	                                    );
	tHandlers[numberOFHandlers] = BarCodeTaskHandler;
	numberOFHandlers++;
	BarCodeTaskHandler->BarCodeReadReqAssci[0] = 0x02;
	BarCodeTaskHandler->BarCodeReadReqAssci[1] = 0x05A;
	BarCodeTaskHandler->BarCodeReadReqAssci[2] = 0x03;
	BarCodeTaskHandler->BarCodeStopReadReqAssci[0] = 0x02;
	BarCodeTaskHandler->BarCodeStopReadReqAssci[1] = 0x059;
	BarCodeTaskHandler->BarCodeStopReadReqAssci[2] = 0x03;
	BarCodeTaskHandler->BarCodeResetConfigReqAssci[0] = 0x02;
	BarCodeTaskHandler->BarCodeResetConfigReqAssci[1] = 'U';
	BarCodeTaskHandler->BarCodeResetConfigReqAssci[2] = '2';
	BarCodeTaskHandler->BarCodeResetConfigReqAssci[3] = 0x03;
	BarCodeTaskHandler->BarCodeDisableGreenLedAssci[0] = 0x02;
	BarCodeTaskHandler->BarCodeDisableGreenLedAssci[1] = 0x5B;
	BarCodeTaskHandler->BarCodeDisableGreenLedAssci[2] = 'D';
	BarCodeTaskHandler->BarCodeDisableGreenLedAssci[3] = '3';
	BarCodeTaskHandler->BarCodeDisableGreenLedAssci[4] = 'E';
	BarCodeTaskHandler->BarCodeDisableGreenLedAssci[5] = 0x03;
}

void BarCodeSerialCommStart(BarCodeHandler_t * BarCodeTaskHandler)
{
    //check that port is initialized
    while (HAL_UART_GetState(BarCodeTaskHandler->port) != HAL_UART_STATE_READY)
    {
    }

    // Receive data from serial port for Serial communication using interrupt
    if(HAL_UART_Receive_IT(BarCodeTaskHandler->port, &BarCodeTaskHandler->dataRX, 1) != HAL_OK)
    {
        while(1)
        {
        }
    }
    BarCodeTaskHandler->u8BufferSize = 0;
/*
    ScaleTaskHandler->u8lastRec = ScaleTaskHandler->u8BufferSize = 0;
    ScaleTaskHandler->u16InCnt = ScaleTaskHandler->u16OutCnt = modH->u16errCnt = 0;
 */
}

/* creation of Serial communication Rx task */
void StartBarCodeRx_task(void *argument)
{
	char *ptr;
	char *str;
	long int  BarCodeRead = 0;
	uint16_t Orientation = 0;

	BarCodeHandler_t * BarCodeHandler = (BarCodeHandler_t * )argument;
	/* Infinite loop */
	for(;;)
	{
	  /* Use ulTaskNotifyTake() to [optionally] block to wait for a task’s notification value to be non-zero.
	   * The task does not consume any CPU time while it is in the Blocked state */
	  ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
	  BarCodeHandler->u8BufferSize = uxQueueMessagesWaiting(BarCodeHandler->QueueSerialHandle);

	  int8_t i8state = getBarCodeRxBuffer(BarCodeHandler);

	  if (i8state < 11)
	  {
		  //The size of the frame is invalid
		  xQueueGenericReset(BarCodeHandler->QueueSerialHandle, pdFALSE);
		  continue;
	  }

	  str = (char*)(BarCodeHandler->au8Buffer);
	  str[i8state-1]=0;
	  BarCodeRead = strtol((str+2),&ptr,10);	// Obtain number
	  str[2] = 0;										// cut number
	  if (strcmp(str,"LA") == 0 )					//check orientation
		  Orientation = eOrientation_LA;
	  else if (strcmp(str,"LB") == 0 )			//check orientation
		  Orientation = eOrientation_LA;
	  else if (strcmp(str,"RA") == 0 )			//check orientation
		  Orientation = eOrientation_RA;
	  else if (strcmp(str,"RB") == 0 )			//check orientation
		  Orientation = eOrientation_RA;
	  else
		  Orientation = 0xffff;
	  osSemaphoreAcquire (*BarCodeHandler->BarCodeSemap_HandleRx , portMAX_DELAY); //before processing the message get the semaphore
	  BarCodeHandler->Data[BarCodeHandler->ModbusMap_ReadingMSB] = (uint16_t) (BarCodeRead>>16); // place the scale reading into Data array at register eModbusMap_ScaleReading
	  BarCodeHandler->Data[BarCodeHandler->ModbusMap_ReadingLSB] = (uint16_t) ((BarCodeRead<<16)>>16); // place the scale reading into Data array at register eModbusMap_ScaleReading
	  BarCodeHandler->Data[BarCodeHandler->ModbusMap_ToteOrientaion] = Orientation;
	  osSemaphoreRelease(*BarCodeHandler->BarCodeSemap_HandleRx); //Release the semaphore
	  osDelay(1);
	}
}

/* creation of Serial communication Tx task */
void StartBarCodeTx_task(void *argument)
{

	BarCodeHandler_t * BarCodeHandler = (BarCodeHandler_t * )argument;
	uint32_t CommandReadFromPLC=0;
	uint32_t cycleTime;

	for(;;)
	{
		CommandReadFromPLC = BarCodeHandler->Data[eModbusMap_ReadCycleTime];
		// if PLC doesn't give cycleTime then eDefaultTxCycleTime is the cycle time
		if (CommandReadFromPLC && osKernelGetTickFreq () != 0)
			cycleTime = (CommandReadFromPLC * osKernelGetTickFreq () / 1000);
		else
			cycleTime = eDefaultTxCycleTime;
		// Wait for a specified time period in ticks, While the system waits, the thread that is calling this function is put into the state WAITING
		//ulTaskNotifyTake(pdTRUE,cycleTime);
		if (bitRead(BarCodeHandler->Data[eModbusMap_ReadRequest],BarCodeHandler->ReadRequestMap_ReadReq))
		{
			sendTxBarCode(BarCodeHandler,0);
		}
		else if (bitRead(BarCodeHandler->Data[eModbusMap_ReadRequest],BarCodeHandler->ReadRequestMap_ReadDisableReq))
		{
			sendTxBarCode(BarCodeHandler,1);
		}
		else if (bitRead(BarCodeHandler->Data[eModbusMap_ReadRequest],BarCodeHandler->ReadRequestMap_ResetReq))
		{
			sendTxBarCode(BarCodeHandler,2);
		}
		else if (bitRead(BarCodeHandler->Data[eModbusMap_ReadRequest],BarCodeHandler->ReadRequestMap_DisableGreenLed))
		{
			sendTxBarCode(BarCodeHandler,3);
		}
		osDelay(cycleTime);
	}

}

HAL_StatusTypeDef ret;
void sendTxBarCode(BarCodeHandler_t *BarCodeHandler, int mode)
{
	switch (mode)
	{
	case 0:
		ret = HAL_UART_Transmit_IT(BarCodeHandler->port, BarCodeHandler->BarCodeReadReqAssci,sizeof( BarCodeHandler->BarCodeReadReqAssci));
		break;
	case 1:
		ret = HAL_UART_Transmit_IT(BarCodeHandler->port, BarCodeHandler->BarCodeStopReadReqAssci,sizeof( BarCodeHandler->BarCodeStopReadReqAssci));
		break;
	case 2:
		ret = HAL_UART_Transmit_IT(BarCodeHandler->port, BarCodeHandler->BarCodeResetConfigReqAssci,sizeof( BarCodeHandler->BarCodeResetConfigReqAssci));
	break;
	case 3:
		ret = HAL_UART_Transmit_IT(BarCodeHandler->port, BarCodeHandler->BarCodeDisableGreenLedAssci,sizeof( BarCodeHandler->BarCodeDisableGreenLedAssci));
	break;
	}
	printf("%d\n", ret);
}


/**
 * @brief
 * This method moves Serial buffer data to the ScaleSerial au8Buffer.
 *
 */
int8_t getBarCodeRxBuffer(BarCodeHandler_t *BarCodeHandler)
{
   int i;

   BarCodeHandler->u8BufferSize = uxQueueMessagesWaiting(BarCodeHandler->QueueSerialHandle);

   for(i = 0; i<  BarCodeHandler->u8BufferSize; i++ )
   {
   	  xQueueReceive(BarCodeHandler->QueueSerialHandle, &BarCodeHandler->au8Buffer[i], 0);
   }

   return BarCodeHandler->u8BufferSize;
}

void vBarCodeTimerCallbackT35(TimerHandle_t *pxTimer)
{
	//Notify that a steam has just arrived
	int i;
	//TimerHandle_t aux;
	for(i = 0; i < numberOFHandlers; i++)
	{

		if( (TimerHandle_t *)tHandlers[i]->xTimerT35 ==  pxTimer )
		{
			xTaskNotify( *tHandlers[i]->BarCodeCommTaskRx, 0, eSetValueWithOverwrite );
		}

	}
}
