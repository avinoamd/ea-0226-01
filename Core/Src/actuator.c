/*
 * actuator.c
 *
 *  Created on: 30 Aug 2021
 *      Author: avinoam.danieli
 */

/*
 * detect move command.
 * execute if not sensor. reset timer counter. state1
 * execute for 30 seconds
 */

#include "main.h"
#include "cmsis_os.h"

void actuator(uint16_t command)
{
	GPIO_PinState val;
	TickType_t start = 0;
	int32_t s;

	if ( ( xTaskGetTickCount() - start ) > 30000 )				// timeout
	{
		HAL_GPIO_WritePin( ACT_RET_GPIO_Port, ACT_RET_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin( ACT_EXT_GPIO_Port, ACT_EXT_Pin, GPIO_PIN_RESET);
		start = xTaskGetTickCount();
		s = 0;
	}
	if ( (command == 0) )
	{
		HAL_GPIO_WritePin( ACT_RET_GPIO_Port, ACT_RET_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin( ACT_EXT_GPIO_Port, ACT_EXT_Pin, GPIO_PIN_RESET);
		s = 0;
	}
	else if ( (command == 0x0040) )								// retract
	{
		val = HAL_GPIO_ReadPin(ACT_DWN_LIM_GPIO_Port, ACT_DWN_LIM_Pin);
		if (val == GPIO_PIN_RESET)
		{
			HAL_GPIO_WritePin( ACT_RET_GPIO_Port, ACT_RET_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin( ACT_EXT_GPIO_Port, ACT_EXT_Pin, GPIO_PIN_RESET);
			if (s == 0)
			{
				start = xTaskGetTickCount();
				s = 1;
			}
		}
		else
		{
			HAL_GPIO_WritePin( ACT_RET_GPIO_Port, ACT_RET_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin( ACT_EXT_GPIO_Port, ACT_EXT_Pin, GPIO_PIN_RESET);
			start = xTaskGetTickCount();
			s = 0;
		}
	}
	else if ( (command == 0x0080) )								// extend
	{
		val = HAL_GPIO_ReadPin(ACT_UP_LIM_GPIO_Port, ACT_UP_LIM_Pin);
		if (val == GPIO_PIN_RESET)
		{
			HAL_GPIO_WritePin( ACT_RET_GPIO_Port, ACT_RET_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin( ACT_EXT_GPIO_Port, ACT_EXT_Pin, GPIO_PIN_SET);
			if (s == 0)
			{
				start = xTaskGetTickCount();
				s = 1;
			}
		}
		else
		{
			HAL_GPIO_WritePin( ACT_RET_GPIO_Port, ACT_RET_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin( ACT_EXT_GPIO_Port, ACT_EXT_Pin, GPIO_PIN_RESET);
			start = xTaskGetTickCount();
			s = 0;
		}
	}

}
