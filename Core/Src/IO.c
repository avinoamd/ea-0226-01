/*
 * IO.c
 *
 *  Created on: Aug 23, 2020
 *      Author: segev.gofin
 */

#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "task.h"
#include "queue.h"
#include "usart.h"
#include "main.h"
#include "timers.h"
#include "semphr.h"
#include "GPIO.h"
#include "IO.h"
/* macro user for masking bit in 16bit variables*/
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define bitSet(value, bit) ((value) |= (1UL << (bit)))
#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitvalue) ((bitvalue) ? bitSet(value, bit) : bitClear(value, bit))

//Sempahore to access the IO Data
const osSemaphoreAttr_t IOSemap_attributes = {
    .name = "IOSemap"
};

/* Definitions for IO_task */
osThreadId_t IO_taskHandle;
const osThreadAttr_t IO_task_attributes = {
  .name = "IO_task",
  .priority = (osPriority_t) osPriorityAboveNormal,
  .stack_size = 128 * 4
};

/**
 * Initialization for IO task
 * */

void InputOutputInit(IOHandler_t * IOHandlerr)
{
	IOHandlerr->myTaskInputOutput = osThreadNew(StartIO_task, IOHandlerr, &IO_task_attributes);
	IOHandlerr->flag =0;
//	IOHandlerr->IOTaskTimerId = osTimerNew(osTimer(IOTaskTimer), osTimerPeriodic, NULL);
//	osTimerStart(IOHandlerr->IOTaskTimerId,1);// timer every 1 ms
}

/* creation of IO_task */
int val, count;
IOHandler_t * IOHandlerr;
int speed;
void actuator(uint16_t command);

void StartIO_task(void *argument)
{
	IOHandlerr = (IOHandler_t * )argument;
	GPIO_PinState val;
  /* Infinite loop */
  for(;;)
  {

	  if (IOHandlerr->IOSemap_Handle != NULL)
	  {
		  osSemaphoreAcquire(*IOHandlerr->IOSemap_Handle , portMAX_DELAY); //before writing the PINs data get the semaphore
		  /* start of Digital Input mapping */
/*
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_EMO,HAL_GPIO_ReadPin(DI_EMO_GPIO_Port, DI_EMO_Pin) ); 				// DI EMO read
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_Interlock,HAL_GPIO_ReadPin(DI_Interlock_GPIO_Port,DI_Interlock_Pin));	// DI Interlock read
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_PushButtonLeftElevator,HAL_GPIO_ReadPin(DI_PushButtonLeftElevator_GPIO_Port,DI_PushButtonLeftElevator_Pin));	// DI Left elevator PB
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_S1LeftElevator,HAL_GPIO_ReadPin(DI_S1LeftElevator_GPIO_Port,DI_S1LeftElevator_Pin));					// DI S1 Left elevator GR in position FR
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_S2LeftElevator,HAL_GPIO_ReadPin(DI_S2LeftElevator_GPIO_Port,DI_S2LeftElevator_Pin));
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_S3LeftElevator,HAL_GPIO_ReadPin(DI_S3LeftElevator_GPIO_Port,DI_S3LeftElevator_Pin));
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_S4LeftElevator,HAL_GPIO_ReadPin(DI_S4LeftElevator_GPIO_Port,DI_S4LeftElevator_Pin));
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_S5LeftElevator,HAL_GPIO_ReadPin(DI_S5LeftElevator_GPIO_Port,DI_S5LeftElevator_Pin));
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_PushButtonRightElevator,HAL_GPIO_ReadPin(DI_PushButtonRightElevator_GPIO_Port,DI_PushButtonRightElevator_Pin));
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_S1RightElevator,HAL_GPIO_ReadPin(DI_S1RightElevator_GPIO_Port,DI_S1RightElevator_Pin));
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_S2RightElevator,HAL_GPIO_ReadPin(DI_S2RightElevator_GPIO_Port,DI_S2RightElevator_Pin));
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_S3RightElevator,HAL_GPIO_ReadPin(DI_S3RightElevator_GPIO_Port,DI_S3RightElevator_Pin));
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_S4RightElevator,HAL_GPIO_ReadPin(DI_S4RightElevator_GPIO_Port,DI_S4RightElevator_Pin));
		  bitWrite(IOHandlerr->Data[eModbusMap_DI1],eDIPinMap_S5RightElevator,HAL_GPIO_ReadPin(DI_S5RightElevator_GPIO_Port,DI_S5RightElevator_Pin));
		  // critical bits
		  bitWrite(IOHandlerr->Data[eModbusMap_DI2],eDIPinMap_OverfillLeftElevator,HAL_GPIO_ReadPin(DI_OverfillLeftElevator_GPIO_Port,DI_OverfillLeftElevator_Pin));
		  bitWrite(IOHandlerr->Data[eModbusMap_DI2],eDIPinMap_OverfillRightElevator,HAL_GPIO_ReadPin(DI_OverfillRightElevator_GPIO_Port,DI_OverfillRightElevator_Pin));
		  bitWrite(IOHandlerr->Data[eModbusMap_DI2],eDIPinMap_SafetyRelay,HAL_GPIO_ReadPin(DI_SafetyRelay_GPIO_Port,DI_SafetyRelay_Pin));	// DI Safety Relay read
*/
//		  bitWrite(IOHandlerr->Data[eModbusMap_DI2],eDIPinMap_heartbeat,!(bitRead(IOHandlerr->Data[eModbusMap_DI2],eDIPinMap_heartbeat)));	// heart bit toggling
		  /* start of Digital Output mapping */
		  HAL_GPIO_WritePin( MCU_OUT_1_GPIO_Port, MCU_OUT_1_Pin, (IOHandlerr->Data[eModbusMap_DO1] & 0x0001) );
		  HAL_GPIO_WritePin( MCU_OUT_2_GPIO_Port, MCU_OUT_2_Pin, (IOHandlerr->Data[eModbusMap_DO1] & 0x0002) );
		  HAL_GPIO_WritePin( MCU_OUT_3_GPIO_Port, MCU_OUT_3_Pin, (IOHandlerr->Data[eModbusMap_DO1] & 0x0004) );
		  HAL_GPIO_WritePin( MCU_OUT_4_GPIO_Port, MCU_OUT_4_Pin, (IOHandlerr->Data[eModbusMap_DO1] & 0x0008) );
		  HAL_GPIO_WritePin( MCU_OUT_5_GPIO_Port, MCU_OUT_5_Pin, (IOHandlerr->Data[eModbusMap_DO1] & 0x0010) );
		  HAL_GPIO_WritePin( MCU_OUT_6_GPIO_Port, MCU_OUT_6_Pin, (IOHandlerr->Data[eModbusMap_DO1] & 0x0020) );

		  actuator(IOHandlerr->Data[eModbusMap_DO1] & 0x00C0);

		  val = HAL_GPIO_ReadPin(MCU_IN_1_GPIO_Port, MCU_IN_1_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x0001;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x0001;
		  val = HAL_GPIO_ReadPin(MCU_IN_2_GPIO_Port, MCU_IN_2_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x0002;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x0002;
		  val = HAL_GPIO_ReadPin(MCU_IN_3_GPIO_Port, MCU_IN_3_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x0004;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x0004;
		  val = HAL_GPIO_ReadPin(MCU_IN_4_GPIO_Port, MCU_IN_4_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x0008;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x0008;
		  val = HAL_GPIO_ReadPin(MCU_IN_5_GPIO_Port, MCU_IN_5_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x0010;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x0010;
		  val = HAL_GPIO_ReadPin(MCU_IN_6_GPIO_Port, MCU_IN_6_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x0020;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x0020;
		  val = HAL_GPIO_ReadPin(MCU_IN_7_GPIO_Port, MCU_IN_7_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x0040;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x0040;
		  val = HAL_GPIO_ReadPin(MCU_IN_8_GPIO_Port, MCU_IN_8_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x0080;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x0080;
		  val = HAL_GPIO_ReadPin(MCU_IN_9_GPIO_Port, MCU_IN_9_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x0100;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x0100;
		  val = HAL_GPIO_ReadPin(MCU_IN_10_GPIO_Port, MCU_IN_10_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x0200;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x0200;
		  val = HAL_GPIO_ReadPin(MCU_IN_11_GPIO_Port, MCU_IN_11_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x0400;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x0400;
		  val = HAL_GPIO_ReadPin(MCU_IN_121_GPIO_Port, MCU_IN_121_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x0800;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x0800;
		  val = HAL_GPIO_ReadPin(MCU_IN_122_GPIO_Port, MCU_IN_122_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x1000;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x1000;
		  val = HAL_GPIO_ReadPin(MCU_IN_13_GPIO_Port, MCU_IN_13_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x2000;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x2000;
		  val = HAL_GPIO_ReadPin(MCU_IN_15_GPIO_Port, MCU_IN_15_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x4000;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x4000;
		  val = HAL_GPIO_ReadPin(MCU_IN_16_GPIO_Port, MCU_IN_16_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI1] &=~ 0x8000;
  		  else IOHandlerr->Data[eModbusMap_DI1] |= 0x8000;

		  val = HAL_GPIO_ReadPin(MCU_IN_17_GPIO_Port, MCU_IN_17_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI2] &=~ 0x0001;
  		  else IOHandlerr->Data[eModbusMap_DI2] |= 0x0001;
		  val = HAL_GPIO_ReadPin(ACT_DWN_LIM_GPIO_Port, ACT_DWN_LIM_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI2] &=~ 0x0002;
  		  else IOHandlerr->Data[eModbusMap_DI2] |= 0x0002;
		  val = HAL_GPIO_ReadPin(ACT_UP_LIM_GPIO_Port, ACT_UP_LIM_Pin);
  		  if (val == GPIO_PIN_RESET) IOHandlerr->Data[eModbusMap_DI2] &=~ 0x0004;
  		  else IOHandlerr->Data[eModbusMap_DI2] |= 0x0004;

/*
		  bitRead(IOHandlerr->Data[eModbusMap_DO1], eDOPinMap_TowerLightRed) ? HAL_GPIO_WritePin(DO_TowerLightRed_GPIO_Port, DO_TowerLightRed_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(DO_TowerLightRed_GPIO_Port, DO_TowerLightRed_Pin, GPIO_PIN_RESET);
		  bitRead(IOHandlerr->Data[eModbusMap_DO1], eDOPinMap_TowerLightOrenge) ? HAL_GPIO_WritePin(DO_TowerLightOrenge_GPIO_Port, DO_TowerLightOrenge_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(DO_TowerLightOrenge_GPIO_Port, DO_TowerLightOrenge_Pin, GPIO_PIN_RESET);
		  bitRead(IOHandlerr->Data[eModbusMap_DO1], eDOPinMap_TowerLightGreen) ? HAL_GPIO_WritePin(DO_TowerLightGreen_GPIO_Port, DO_TowerLightGreen_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(DO_TowerLightGreen_GPIO_Port, DO_TowerLightGreen_Pin, GPIO_PIN_RESET);
		  bitRead(IOHandlerr->Data[eModbusMap_DO1], eDOPinMap_TowerLightBlue) ? HAL_GPIO_WritePin(DO_TowerLightBlue_GPIO_Port, DO_TowerLightBlue_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(DO_TowerLightBlue_GPIO_Port, DO_TowerLightBlue_Pin, GPIO_PIN_RESET);
		  bitRead(IOHandlerr->Data[eModbusMap_DO1], eDOPinMap_GreenValidationLightLeftElevator) ? HAL_GPIO_WritePin(DO_GreenValidationLightLeftElevator_GPIO_Port, DO_GreenValidationLightLeftElevator_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(DO_GreenValidationLightLeftElevator_GPIO_Port, DO_GreenValidationLightLeftElevator_Pin, GPIO_PIN_RESET);
		  bitRead(IOHandlerr->Data[eModbusMap_DO1], eDOPinMap_RedValidationLightLeftElevator) ? HAL_GPIO_WritePin(DO_RedValidationLightLeftElevator_GPIO_Port, DO_RedValidationLightLeftElevator_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(DO_RedValidationLightLeftElevator_GPIO_Port, DO_RedValidationLightLeftElevator_Pin, GPIO_PIN_RESET);
		  bitRead(IOHandlerr->Data[eModbusMap_DO1], eDOPinMap_GreenValidationLightRightElevator) ? HAL_GPIO_WritePin(DO_GreenValidationLightRightElevator_GPIO_Port, DO_GreenValidationLightRightElevator_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(DO_RedValidationLightRightElevator_GPIO_Port, DO_GreenValidationLightRightElevator_Pin, GPIO_PIN_RESET);
		  bitRead(IOHandlerr->Data[eModbusMap_DO1], eDOPinMap_RedValidationLightRightElevator) ? HAL_GPIO_WritePin(DO_RedValidationLightRightElevator_GPIO_Port, DO_RedValidationLightRightElevator_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(DO_GreenValidationLightRightElevator_GPIO_Port, DO_RedValidationLightRightElevator_Pin, GPIO_PIN_RESET);
		  bitRead(IOHandlerr->Data[eModbusMap_DO1], eDOPinMap_SafetyRelayReset) ? HAL_GPIO_WritePin(DO_SafetyRelayReset_GPIO_Port, DO_SafetyRelayReset_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(DO_SafetyRelayReset_GPIO_Port, DO_SafetyRelayReset_Pin, GPIO_PIN_RESET);
*/

		  osSemaphoreRelease(*IOHandlerr->IOSemap_Handle); //Release the semaphore
		  //val = HAL_GPIO_ReadPin(DI_S1LeftElevator_GPIO_Port,DI_S1LeftElevator_Pin);
		  osDelay(25);
     }


  }
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	UNUSED(GPIO_Pin);
}
