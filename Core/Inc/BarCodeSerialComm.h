/*
 * BarCodeSerialComm.h
 *
 *  Created on: Sep 14, 2020
 *      Author: segev.gofin
 */

#ifndef INC_BARCODESERIALCOMM_H_
#define INC_BARCODESERIALCOMM_H_


typedef struct
{
	uint16_t * Data; // pointer to Data from/to modbus
	UART_HandleTypeDef *port; 				// HAL Serial Port handler
	/* FreeRTOS RX (CMSIS v2) components */
	osThreadId_t *BarCodeCommTaskRx;			// Task Serial communication Rx
	osSemaphoreId_t *BarCodeSemap_HandleRx;	// Sempahore to access the Data from/to modbus
	osMessageQueueId_t QueueSerialHandle;	// Queue Serial RX
	xTimerHandle xTimerT35;					// Timer RX Scale serial communication
	xTimerHandle xTimerTimeout;				// Timer MasterTimeout

	uint8_t au8Buffer[MAX_BUFFER]; 		// Serial buffer for communication
	uint8_t u8BufferSize;

	uint8_t dataRX;
	// uint8_t ReadOrWriteMode; // Read = 0, Write = 1

	/* FreeRTOS TX (CMSIS v2) components */
	osThreadId_t *BarCodeCommTaskTx;			// Task Serial communication
	osSemaphoreId_t *BarCodeSemap_HandleTx;	// Sempahore to access the Data from/to modbus
	uint8_t BarCodeReadReqAssci[3];
	uint8_t BarCodeStopReadReqAssci[3];
	uint8_t BarCodeResetConfigReqAssci[4];
	uint8_t BarCodeDisableGreenLedAssci[8];
	uint8_t ReadRequestMap_ReadReq;
	uint8_t ReadRequestMap_ReadDisableReq;
	uint8_t ReadRequestMap_ResetReq;
	uint8_t ReadRequestMap_DisableGreenLed;
	uint8_t DefaultTxCycleTime;
	uint8_t ModbusMap_ReadingMSB;
	uint8_t ModbusMap_ReadingLSB;
	uint8_t ModbusMap_ToteOrientaion;
}
BarCodeHandler_t;

extern BarCodeHandler_t *tHandlers[MAX_M_HANDLERS];

void BarCodeSerialCommInit(BarCodeHandler_t *);
void BarCodeSerialCommStart(BarCodeHandler_t * );
void StartBarCodeRx_task(void *);
void StartBarCodeTx_task(void *);
void sendTxBarCode(BarCodeHandler_t *,int);
int8_t getBarCodeRxBuffer(BarCodeHandler_t *);
void vBarCodeTimerCallbackT35(TimerHandle_t *);
#endif /* INC_BARCODESERIALCOMM_H_ */
