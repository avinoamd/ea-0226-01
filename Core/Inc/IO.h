/*
 * IO.h
 *
 *  Created on: Aug 23, 2020
 *      Author: segev.gofin
 */

#ifndef INC_IO_H_
#define INC_IO_H_

typedef struct
{
	uint16_t * Data;
	//FreeRTOS components
	//Task Input Output Task
	osThreadId_t *myTaskInputOutput;
	//Sempahore to access the IO Data
	osSemaphoreId_t *IOSemap_Handle;
	uint8_t flag;
}
IOHandler_t;

void InputOutputInit(IOHandler_t *);
void StartIO_task(void *);
#endif /* INC_IO_H_ */
