/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "Modbus.h"
#include "IO.h"
#include "BarCodeSerialComm.h"

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
extern modbusHandler_t ModbusH;
extern BarCodeHandler_t BarCodeSerial1;
extern BarCodeHandler_t BarCodeSerial2;
extern BarCodeHandler_t BarCodeSerial3;

enum ModbusMapEnum
{
	// read
	eModbusMap_DI1,									// 0
	eModbusMap_DI2,									// 1
	eModbusMap_BarCode_1_ReadingMSB,
	eModbusMap_BarCode_1_ReadingLSB,				// 3
	eModbusMap_BarCode_1_ToteOrientaion,
	eModbusMap_BarCode_2_ReadingMSB,
	eModbusMap_BarCode_2_ReadingLSB,				// 6
	eModbusMap_BarCode_2_ToteOrientaion,
	eModbusMap_BarCode_3_ReadingMSB,
	eModbusMap_BarCode_3_ReadingLSB,				// 9
	eModbusMap_BarCode_3_ToteOrientaion,
	eModbusMap_Heartbeat,
	eModbusMap_SWVer,									// 12

//	eModbusMap_McuAdress,
	// write
	eModbusMap_ReadCycleTime, 			//cycle time to ask for reading from PLC
	eModbusMap_DO1,
	eModbusMap_ReadRequest,							// 15

};

enum ReadRequestMap
{
	eReadRequestMap_ReadReq = 0,		// Bit from PLC for asking reading
	eReadRequestMap_Scale_2,
	eReadRequestMap_Scale_3
};

/* A number that if the address in power up is equal to then MCU goes into test mode */
enum {eGoIntoTestModeAddress = 2};

typedef enum
{
	eTest_state_RS232_Transmit,
	eTest_state_RS232_Receive,
	eTest_state_RS232_Compare,
	eTest_state_Blinker
} eTest_state;

enum {eDefaultTxCycleTime = 425};

enum BarCodeRelatedEnum
{
	eReadRequestMap_Barcode_1_ReadReq=0,					// 0 1
	eReadRequestMap_Barcode_1_AllowReadDisableReq,		// 1 2
	eReadRequestMap_Barcode_1_ResetReq,						// 2 4
	eReadRequestMap_Barcode_1_BarCodeDisableGreenLed,	// 3 8
	eReadRequestMap_Barcode_2_ReadReq,						// 4 16
	eReadRequestMap_Barcode_2_AllowReadDisableReq,		// 5 32
	eReadRequestMap_Barcode_2_ResetReq,						// 6 64
	eReadRequestMap_Barcode_2_BarCodeDisableGreenLed,	// 7 128
	eReadRequestMap_Barcode_3_ReadReq,						// 8 256
	eReadRequestMap_Barcode_3_AllowReadDisableReq,		// 9 512
	eReadRequestMap_Barcode_3_ResetReq,						// 10 1024
	eReadRequestMap_Barcode_3_BarCodeDisableGreenLed,	// 11 2048
//	eDefaultTxCycleTime = 1000									// 200
};

/* Digital input mapping */
enum DIPinMapping 
{
	eDIPinMap_heartbeat				=	3
};
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MCU_OUT_4_Pin GPIO_PIN_2
#define MCU_OUT_4_GPIO_Port GPIOE
#define MCU_OUT_3_Pin GPIO_PIN_3
#define MCU_OUT_3_GPIO_Port GPIOE
#define MCU_OUT_2_Pin GPIO_PIN_4
#define MCU_OUT_2_GPIO_Port GPIOE
#define MCU_OUT_1_Pin GPIO_PIN_5
#define MCU_OUT_1_GPIO_Port GPIOE
#define DEBUG_LED_Pin GPIO_PIN_13
#define DEBUG_LED_GPIO_Port GPIOC
#define P5V_SCANNER_EN_Pin GPIO_PIN_0
#define P5V_SCANNER_EN_GPIO_Port GPIOC
#define P5V_SCANNER_PG_Pin GPIO_PIN_1
#define P5V_SCANNER_PG_GPIO_Port GPIOC
#define ACT_RET_Pin GPIO_PIN_9
#define ACT_RET_GPIO_Port GPIOE
#define ACT_EXT_Pin GPIO_PIN_11
#define ACT_EXT_GPIO_Port GPIOE
#define MCU_IN_17_Pin GPIO_PIN_12
#define MCU_IN_17_GPIO_Port GPIOB
#define MCU_IN_16_Pin GPIO_PIN_13
#define MCU_IN_16_GPIO_Port GPIOB
#define MCU_IN_15_Pin GPIO_PIN_14
#define MCU_IN_15_GPIO_Port GPIOB
#define MCU_IN_1_Pin GPIO_PIN_15
#define MCU_IN_1_GPIO_Port GPIOB
#define MCU_IN_1_EXTI_IRQn EXTI15_10_IRQn
#define MCU_IN_2_Pin GPIO_PIN_8
#define MCU_IN_2_GPIO_Port GPIOD
#define MCU_IN_2_EXTI_IRQn EXTI9_5_IRQn
#define MCU_IN_3_Pin GPIO_PIN_9
#define MCU_IN_3_GPIO_Port GPIOD
#define MCU_IN_3_EXTI_IRQn EXTI9_5_IRQn
#define MCU_IN_13_Pin GPIO_PIN_10
#define MCU_IN_13_GPIO_Port GPIOD
#define MCU_IN_13_EXTI_IRQn EXTI15_10_IRQn
#define MCU_IN_4_Pin GPIO_PIN_11
#define MCU_IN_4_GPIO_Port GPIOD
#define MCU_IN_4_EXTI_IRQn EXTI15_10_IRQn
#define MCU_IN_121_Pin GPIO_PIN_12
#define MCU_IN_121_GPIO_Port GPIOD
#define MCU_IN_121_EXTI_IRQn EXTI15_10_IRQn
#define MCU_IN_5_Pin GPIO_PIN_13
#define MCU_IN_5_GPIO_Port GPIOD
#define MCU_IN_5_EXTI_IRQn EXTI15_10_IRQn
#define MCU_IN_6_Pin GPIO_PIN_14
#define MCU_IN_6_GPIO_Port GPIOD
#define MCU_IN_6_EXTI_IRQn EXTI15_10_IRQn
#define MCU_IN_7_Pin GPIO_PIN_6
#define MCU_IN_7_GPIO_Port GPIOC
#define MCU_IN_7_EXTI_IRQn EXTI9_5_IRQn
#define MCU_IN_8_Pin GPIO_PIN_8
#define MCU_IN_8_GPIO_Port GPIOC
#define MCU_IN_9_Pin GPIO_PIN_9
#define MCU_IN_9_GPIO_Port GPIOC
#define MCU_IN_10_Pin GPIO_PIN_8
#define MCU_IN_10_GPIO_Port GPIOA
#define MCU_IN_11_Pin GPIO_PIN_11
#define MCU_IN_11_GPIO_Port GPIOA
#define RS485_DE_Pin GPIO_PIN_12
#define RS485_DE_GPIO_Port GPIOA
#define MCU_IN_122_Pin GPIO_PIN_11
#define MCU_IN_122_GPIO_Port GPIOC
#define MCU_OUT_5_Pin GPIO_PIN_4
#define MCU_OUT_5_GPIO_Port GPIOD
#define MCU_OUT_6_Pin GPIO_PIN_5
#define MCU_OUT_6_GPIO_Port GPIOD
#define ACT_DWN_LIM_Pin GPIO_PIN_8
#define ACT_DWN_LIM_GPIO_Port GPIOB
#define ACT_UP_LIM_Pin GPIO_PIN_9
#define ACT_UP_LIM_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
